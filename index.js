const express = require('express')
const app = express()
const port = process.argv[2]   //3000

app.get('/', (req, res) => res.send('<html><head><title>Port ' + port + '</title></head><body><h1>Hello World! ' + port + '</h1></body></html>'))

app.listen(port, () => console.log(`Example app listening on port ${port}!`))